from django.shortcuts import render
from django.http import HttpResponse

from api.models import Todo

def js(request):
    latest_todo_list = Todo.objects.order_by('-pub_date')[:5]
    context = {'latest_todo_list': latest_todo_list}
    return render(request, 'frontend/index.html', context) 

def app(request):
    latest_todo_list = Todo.objects.order_by('-pub_date')[:5]
    context = {'latest_todo_list': latest_todo_list}
    return render(request, 'frontend/app.html', context) 
 