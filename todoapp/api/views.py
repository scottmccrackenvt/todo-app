from django.shortcuts import render
from rest_framework.generics import ListAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView
from api.serializers import TodoSerializer
from api.models import Todo

class todoList(ListAPIView):
    queryset = Todo.objects.all().order_by('completed', '-pub_date')
    serializer_class = TodoSerializer

class todoCreate(CreateAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer

class todoUpdate(UpdateAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer

class todoDelete(DestroyAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer