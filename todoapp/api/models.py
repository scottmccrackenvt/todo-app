import datetime

from django.db import models
from django.utils import timezone

class Todo(models.Model):
    title = models.CharField(max_length=240)
    completed = models.BooleanField(default=False)
    pub_date = models.DateTimeField('Date created', auto_now_add=True)

    def __str__(self):
        return self.title
    
    def created_today(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)