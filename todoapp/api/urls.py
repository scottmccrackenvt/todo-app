from django.urls import path
from api import views

app_name = 'api'
urlpatterns = [
    path('', views.todoList.as_view(), name='todo-list'),
    path('create/', views.todoCreate.as_view(), name='todo-create'),
    path('delete/<str:pk>/', views.todoDelete.as_view(), name='todo-delete'),
    path('update/<str:pk>/', views.todoUpdate.as_view(), name='todo-update'),
]